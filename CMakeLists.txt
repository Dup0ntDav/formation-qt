# pour supporter C++17, cmake 3.9 est nécessaire
cmake_minimum_required(VERSION 3.9 FATAL_ERROR)

# options de configuration du projet
option(BUILD_TESTING "Build unit tests" OFF)
option(ENABLE_COVERAGE "Enable code coverage (lcov must be installed and BUILD_TESTING must be ON)" OFF)

project(formation-qt LANGUAGES CXX)

# activation de C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON) # force C++17
set(CMAKE_CXX_EXTENSIONS OFF) # désactiver les extensions spécifiques à chaque compilateur

# activation du traitement automatisé des fichiers Qt spéciaux (moc, qrc)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

# recherche des packages Qt nécessaires à l'application
find_package(Qt5 COMPONENTS Core Quick REQUIRED)

# ajustement des options de compilation (activation d'un niveau de warning élevé)
if (MSVC)
    # VC++
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4 /WX /permissive-")
else()
    # gcc, clang
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -pedantic -Wextra -Wshadow -Wnon-virtual-dtor -Wold-style-cast -Wunused -Wno-date-time -Werror")
endif()

# activation des tests unitaires?
if (BUILD_TESTING)
    enable_testing()
endif()

# ajout du code source
add_subdirectory(src)
add_subdirectory(third_party)
