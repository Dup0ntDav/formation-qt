#!/bin/bash
# script used to format the code with clang-format

set -e

# ensure clang-format is available
CFORMAT_TOOL=clang-format-8
which $CFORMAT_TOOL > /dev/null || (echo "Error: clang-format is missing (sudo apt-get install $CFORMAT_TOOL)" && exit 1)

cformat() {
    DEST_DIR=$1
    echo "Running $CFORMAT_TOOL on $DEST_DIR"
    find $DEST_DIR -name "*.cc" -or -name "*.cpp" -or -name "*.c" -or -name "*.h" -or -name "*.hpp" | xargs $CFORMAT_TOOL -i
}

ROOTDIR=`dirname "$0"`

# source directory to format
cformat $ROOTDIR/src

echo "Done!"
