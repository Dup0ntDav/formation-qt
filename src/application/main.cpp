#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTimer>

#include "logging.h"
#include "models/LoginModel.h"

static QString getCompilationDate() {
    return QString::fromLatin1(__DATE__);
}

static void attachFakeLoginController(LoginModel & model) {
    QObject::connect(&model, &LoginModel::startLogin, [&model](QString name, QString pwd) {
        qDebug() << "Login:" << name << pwd;
        model.setLoginInProgress(true);
        QTimer::singleShot(2000, [&model] { model.setLoggedIn(true); });
    });

    auto onLogout = [&model] {
        model.setLoginInProgress(false);
        model.setLoggedIn(false);
    };
    QObject::connect(&model, &LoginModel::logout, onLogout);
}

int main(int argc, char * argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QCoreApplication::setApplicationName("QML Demo");
    QCoreApplication::setApplicationVersion(getCompilationDate());

    initLogger();

    QQmlApplicationEngine engine;

    // exposer au moteur QML une instance de LoginModel ayant comme nom "loginModel"
    LoginModel modelLogin;
    engine.rootContext()->setContextProperty("loginModel", &modelLogin);

    // attacher un controlleur
    attachFakeLoginController(modelLogin);

    // lancer le QML
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return 1;
    }

    return app.exec();
}
