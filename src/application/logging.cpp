#include <QCoreApplication>
#include <QDateTime>
#include <QDir>

#include <iostream>
#include <memory>

static std::unique_ptr<QFile> s_logFile;

static bool appendLogMsg(QString msg) {
    if (!s_logFile) {
        return false;
    }

    if (!msg.endsWith("\n")) {
        msg += "\n";
    }
    if (!s_logFile->write(msg.toUtf8())) {
        return false;
    }
    s_logFile->flush();
    return true;
}

static QString getQtLogLevel(QtMsgType type) {
    switch (type) {
    case QtDebugMsg:
        return "DEBUG";
    case QtWarningMsg:
        return "WARNING";
    case QtCriticalMsg:
        return "CRITICAL";
    case QtFatalMsg:
        return "FATAL";
    case QtInfoMsg:
        return "INFO";
    }
    return "???";
}

static void qtLogMsgHandler(QtMsgType type, const QMessageLogContext & ctx, const QString & msg) {
    // display on console
    const QString timeStamp = QDateTime::currentDateTime().toLocalTime().toString("yyyy-MM-dd hh:mm:ss");
    const QString level     = QString{ "[%1]" }.arg(getQtLogLevel(type)).leftJustified(10);
    const QString finalMsg  = timeStamp + " " + level + " " + msg;
    std::cout << finalMsg.toStdString() << std::endl;

    // append to log file
    appendLogMsg(finalMsg);

    if (type == QtWarningMsg) {
        // in debug, help to catch issues in qml code
        Q_ASSERT(!QString{ ctx.file }.contains(".qml"));
    }
}

void initLogger() {
    QDir dir = QCoreApplication::applicationDirPath();
    // create log dir if missing
    if (!dir.exists("logs")) {
        dir.mkdir("logs");
    }
    if (!dir.cd("logs")) {
        throw std::runtime_error{ "failed to create output logs dir" };
    }

    // create new log file
    const QString timeStamp = QDateTime::currentDateTime().toLocalTime().toString("yyyy-MM-dd hh-mm-ss");
    const QString logFilePath =
        dir.filePath(QString{ "%1-%2.log" }.arg(QCoreApplication::applicationName(), timeStamp));
    auto logFile = std::make_unique<QFile>(logFilePath);
    if (!logFile->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
        throw std::runtime_error{ "failed to create log file" };
    }
    s_logFile = std::move(logFile);

    // verify
    if (!appendLogMsg("Starting application")) {
        throw std::runtime_error{ "failed to init log file" };
    }

    // ready to log!
    qInstallMessageHandler(&qtLogMsgHandler);
}
