import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.13

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("%1 (%2)").arg(Qt.application.name).arg(Qt.application.version)

    // Modèle au sens MVC
    property var model: loginModel

    Connections {
        target: pageLogin
        onStartLogin: model.startLogin(userName, password)
    }

    // Vue au sens MVC
    SwipeView {
        id: swipeView
        anchors.fill: parent
        interactive: false
        focus: true
        contentItem.focus: true
        currentIndex: computeActivePageNumber()

        function computeActivePageNumber() {
            if (model.loggedIn) return 2;
            if (model.loginInProgress) return 1;
            return 0;
        }

        PageLogin {
            id: pageLogin
        }

        PageLoginInProgress {
            id: pageLoginInProgress
        }

        PageMnemo {
            id: pageMnemo
        }
    }

    PageIndicator {
        id: indicator
        count: swipeView.count
        currentIndex: swipeView.currentIndex
        anchors.bottom: swipeView.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            Action {
                text: qsTr("Logout")
                enabled: model.loggedIn
                onTriggered: model.logout()
            }
            MenuSeparator { }
            Action {
                text: qsTr("&Quit")
                onTriggered: Qt.quit()
                shortcut: StandardKey.Quit
            }
        }
    }

    Component.onCompleted: console.log("main.onCompleted")
}
