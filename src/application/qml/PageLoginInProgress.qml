import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

Item {
    id: element
    BusyIndicator {
        id: busyIndicator
        x: 167
        anchors.topMargin: 20
        anchors.top: label.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Label {
        id: label
        x: 279
        y: 142
        color: "#1130cf"
        text: qsTr("Login in progress...")
        font.pointSize: 14
        anchors.verticalCenterOffset: -10
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Button {
        id: button
        x: 270
        y: 353
        text: qsTr("Test error")
        onClicked: {
            dlgLoginError.text = qsTr("Test erreur")
            dlgLoginError.open()
        }
    }

    Dialog {
        id: dlgLoginError
        property alias text: dlgLabel.text

        modal: true
        anchors.centerIn: parent
        title: qsTr("Login failure")
        standardButtons: DialogButtonBox.Close

        Label {
            id: dlgLabel
            font.pixelSize: 14
            color: "red"
        }
    }
}





/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:1;anchors_y:226}
}
 ##^##*/
