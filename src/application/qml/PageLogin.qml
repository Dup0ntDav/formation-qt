import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

Item {
    signal startLogin(string userName, string password)

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: 200
        spacing: 10
        // clip: true

        InputTextField {
            id: textFieldUserName
            width: parent.width
            placeholderText: qsTr("Enter your name")
            minInputLength: 1
            onSubmitted: parent.login()
        }

        InputTextField {
            id: textFieldPassword
            width: parent.width
            placeholderText: qsTr("Enter your password")
            echoMode: TextInput.Password
            minInputLength: 1
            onSubmitted: {
                parent.login()
            }
        }

        Button {
            id: button
            width: parent.width
            enabled: textFieldUserName.inputValid && textFieldPassword.inputValid
            text: qsTr("Login")
            onClicked: parent.login()
        }

        function login() {
            startLogin(textFieldUserName.text, textFieldPassword.text)
        }
    }

    onActiveFocusChanged: {
        console.log("active focus")
        if (activeFocus) {
            textFieldUserName.forceActiveFocus()
            // reset fields
            textFieldPassword.clear()
        }
    }
}
